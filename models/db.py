# -*- coding: utf-8 -*-

#########################################################################
## This scaffolding model makes your app work on Google App Engine too
## File is released under public domain and you can use without limitations
#########################################################################

## if SSL/HTTPS is properly configured and you want all HTTP requests to
## be redirected to HTTPS, uncomment the line below:
#request.requires_https()
from gluon.contrib.redis_session import RedisSession
from gluon.contrib.redis_cache import RedisCache

if not request.env.web2py_runtime_gae:
    ## if NOT running on Google App Engine use SQLite or other DB
    db = DAL('postgres://zbysiu:ciul@localhost/spyzlom')
    #db = DAL('sqlite://storage.sqlite', pool_size=1, check_reserved=['all'])

    ## connect to Google BigTable (optional 'google:datastore://namespace')
    #db = DAL('google:datastore')
    ## store sessions and tickets there
    # session.connect(request, response, db=db)
    sessiondb = RedisSession('localhost:6379',db=0, session_expiry=False)
    session.connect(request, response, db = sessiondb)
    cache.redis = RedisCache('localhost:6379',db=None, debug=False)
    cache.ram = cache.disk = cache.redis
    ## or store session in Memcache, Redis, etc.
    ## from gluon.contrib.memdb import MEMDB
    ## from google.appengine.api.memcache import Client
    ## session.connect(request, response, db = MEMDB(Client()))

## by default give a view/generic.extension to all actions from localhost
## none otherwise. a pattern can be 'controller/function.extension'
response.generic_patterns = ['*'] if request.is_local else []
## (optional) optimize handling of static files
# response.optimize_css = 'concat,minify,inline'
# response.optimize_js = 'concat,minify,inline'

#########################################################################
## Here is sample code if you need for
## - email capabilities
## - authentication (registration, login, logout, ... )
## - authorization (role based authorization)
## - services (xml, csv, json, xmlrpc, jsonrpc, amf, rss)
## - old style crud actions
## (more options discussed in gluon/tools.py)
#########################################################################

from gluon.tools import Auth, Crud, Service, PluginManager, prettydate
auth = Auth(db)
crud, service, plugins = Crud(db), Service(), PluginManager()

auth.settings.extra_fields['auth_user'] = [
    Field('f_team', 'string', label=T('Team')),
    Field('f_vehicle', 'string', label=T('Vehicle'), default='Żuk'),
    Field('f_color', 'string', label=T('Icon Colour'), default='black'),
    Field('f_vehicle_type', 'string', label=T('Icon type'), default='car'),
    Field('f_crew_size', 'integer', label=T('Crew size'), default=1),
    Field('f_city', 'string', label=T('City')),
    Field('f_www', 'string', label=T('Website')),
    Field('f_funpage', 'string', label='Fanpage'),
    Field('f_contact', 'text', label=T('From you to others'),
          comment=T('Visible to the public')),
    Field('f_contact_phone', 'string', label=T('Contact phone'),
          comment=T('Visible only to logged in Złombol members')),
    Field('f_contact_email', 'string', label=T('Contact email'),
          comment=T('Visible only to logged in Złombol members')),
    Field('f_pic', 'upload', label=T('Picture'), autodelete=True),
    Field('f_thumb_small', 'upload', autodelete=True,
          writable=False, readable=False,
          compute=lambda r: makeThumbnail(r.f_pic, 'small')),
    Field('f_thumb_big', 'upload', autodelete=True,
          writable=False, readable=False,
          compute=lambda r: makeThumbnail(r.f_pic, 'big')),
    Field('f_failure', 'boolean', label=T('We have a failure'),
          comment=T('Check if you are looking for help or would like to inform about a failure.')),
    Field('f_failure_desc', 'text', label=T('Problem description'),
          comment=T('Describe what is the problem. Maybe someone can help you.'))]


def makeThumbnail(filename, prefix):
    size = None
    quality = None
    if prefix == 'small':
        size = (400, 400)
        quality = 65
    if prefix == 'big':
        size = (1024, 1024)
        quality = 80
    try:
        from PIL import Image
        im = Image.open(request.folder + 'uploads/' + filename)
        if im.mode != "RGB":
            im = im.convert("RGB")
    except:
        return
    width, height = im.size
    if height > 1.5 * width:
        size = (size[0]*1.5, size[1]*1.5)
    im.thumbnail(size, Image.ANTIALIAS)
    thumbName = filename.replace('f_pic', 'f_thumb_' + prefix)
    im.save(request.folder + 'uploads/' + thumbName, 'jpeg', quality=80)
    return thumbName

## create all tables needed by auth if not custom tables
# from extended_login_form import ExtendedLoginForm
# from extended_login_form import FaceBookAccount

# facebook_login = FaceBookAccount(globals())
# extended_login_form = ExtendedLoginForm(
#     auth, facebook_login, signals=['token'])
#auth.settings.login_form = facebook_login
auth.define_tables(username=False, signature=False)

db.auth_user.f_team.requires = IS_NOT_IN_DB(db, db.auth_user.f_team)
db.auth_user.f_color.requires = IS_IN_SET({'aqua': T('Aqua'),
                                          'black': T('Black'),
                                          'blue': T('Blue'),
                                          'blueviolet': T('Blueviolet'),
                                          'brown': T('Brown'),
                                          'burlywood': T('Burly wood'),
                                          'cadetblue': T('Cadetblue'),
                                          'chocolate': T('Chocolate'),
                                          'coral': T('Coral'),
                                          'crimson': T('Crimson'),
                                          'darkgreen': T('Darkgreen'),
                                          'fuchsia': T('Fuchsia'),
                                          'gold': T('Gold'),
                                          'gray': T('Gray'),
                                          'khaki': T('Khaki'),
                                          'lime': T('Lime'),
                                          'pink': T('Pink'),
                                          'red': T('Red'),
                                          'sea': T('Sea'),
                                          'yellowgreen': T('Yellowgreen')},
                                          zero=None)
db.auth_user.f_vehicle_type.requires = IS_IN_SET({'jeep': T('Jeep'),
                                                 'fourbyfour': T('4x4'),
                                                 'sportscar': T('Sport car'),
                                                 'convertible':
                                                 T('Convertible'),
                                                 'bus': T('Bus'),
                                                 'vespa': T('Scooter'),
                                                 'van': T('Van'),
                                                 'truck3': T('Truck'),
                                                 'motorcycle': T('Motor'),
                                                 'ducati-diavel': T('Super motor'),
                                                 'taxi': T('Taxi'),
                                                 'sportutilityvehicle':
                                                 T('SUV'),
                                                 'bulldozer': T('Bulldozer')},
                                                 zero=None)
db.auth_user.f_vehicle.requires = IS_NOT_EMPTY()
db.auth_user.f_crew_size.requires = IS_NOT_EMPTY()
db.auth_user.f_www.requires = IS_EMPTY_OR(IS_URL(
    allowed_schemes=['https', 'http']))
db.auth_user.f_funpage.requires = IS_EMPTY_OR(IS_URL(
    allowed_schemes=['https', 'http']))
db.auth_user.f_pic.requires = IS_EMPTY_OR(IS_IMAGE(
    extensions=('gif', 'jpeg', 'png'),
    maxsize=(10000, 10000),
    minsize=(400, 400),
    error_message=T("""
        Allowed images: *.jpg, *gif and *.png.
        Max: 10000x10000
        Min: 400x400
        """)))
db.auth_user.f_contact_email.requires = IS_EMPTY_OR(
    IS_EMAIL(error_message=T('Invalid email')))
db.auth_user.f_contact_phone.requires = IS_EMPTY_OR(
    IS_MATCH(
        r"^([\+]){1}([0-9]{2}) \d\d\d \d\d\d \d\d\d$",
        error_message=T('Not a phone number. Example: +48 666 666 666')))

## configure email
mail = auth.settings.mailer
mail.settings.server = 'smtp.zoho.com:587'
mail.settings.sender = 'noreply@gdzieonikurdesa.pl'
mail.settings.login = 'noreply@gdzieonikurdesa.pl:samsung0'

## configure auth policy
auth.settings.registration_requires_verification = False
auth.settings.registration_requires_approval = False
auth.settings.reset_password_requires_verification = False
auth.settings.allow_basic_login = True

## if you need to use OpenID, Facebook, MySpace, Twitter, Linkedin, etc.
## register with janrain.com, write your domain:api_key in private/janrain.key
from gluon.contrib.login_methods.rpx_account import use_janrain
use_janrain(auth, filename='private/janrain.key')

#########################################################################
## Define your tables below (or better in another model file) for example
##
## >>> db.define_table('mytable',Field('myfield','string'))
##
## Fields can be 'string','text','password','integer','double','boolean'
##       'date','time','datetime','blob','upload', 'reference TABLENAME'
## There is an implicit 'id integer autoincrement' field
## Consult manual for more options, validators, etc.
##
## More API examples for controllers:
##
## >>> db.mytable.insert(myfield='value')
## >>> rows=db(db.mytable.myfield=='value').select(db.mytable.ALL)
## >>> for row in rows: print row.id, row.myfield
#########################################################################

## after defining tables, uncomment below to enable auditing
# auth.enable_record_versioning(db)

db.define_table(
    'samples',
    Field('f_timestamp', 'bigint', writable=False, readable=False),
    Field('f_latitude', 'double', writable=False, readable=False),
    Field('f_longitude', 'double', writable=False, readable=False),
    Field('f_altitude', 'double', writable=False, readable=False),
    Field('f_speed', 'float', default=0, writable=False, readable=False),
    Field('f_accuracy', 'float', default=0, writable=False, readable=False),
    Field('f_bearing', 'float', default=0, writable=False, readable=False),
    Field('f_provider', 'string', default='unknown',
          writable=False, readable=False),
    auth.signature,
    format='%(f_timestamp)s')

db.samples.f_timestamp.requires = IS_NOT_EMPTY()
db.samples.f_latitude.requires = IS_NOT_EMPTY()
db.samples.f_longitude.requires = IS_NOT_EMPTY()

db.define_table(
    'kml',
    Field('f_date', 'date'),
    Field('f_user_id', db.auth_user),
    Field('f_samples_nb', 'integer'),
    Field('f_kml', 'upload', autodelete=True),
    Field('f_kmz', 'upload', autodelete=True))

#db.kml.truncate()
