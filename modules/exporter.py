# -*- coding: utf-8 -*-

from gluon import current
from datetime import timedelta
from datetime import datetime
from datetime import date
from datetime import time


def toTimestamp(datetime):
    import calendar
    return calendar.timegm(datetime.timetuple())*1000


def toDatetime(timestamp):
    return datetime.utcfromtimestamp(timestamp/1e3)


class Exporter(object):
    def __init__(self, db):
        self.db = db
        self.color = None

    def buildUserMaps(self, userid, color, fullproc=False, overwrite=False):
        db = self.db
        self.color = color
        smax = db.samples.f_timestamp.max()
        smin = db.samples.f_timestamp.min()
        q = db(db.samples.modified_by == userid).select(smax, smin).first()
        start = toDatetime(q[smin])
        stop = toDatetime(q[smax])
        if not fullproc:
            start = datetime.now() - timedelta(days=2)
        for d in self.daterange(start, stop):
            kmlRow = db((db.kml.f_date == d.date()) &
                        (db.kml.f_user_id == userid)).select().first()

            if kmlRow is None or d.date() >= date.today() - timedelta(days=1):
                self.buildKML(kmlRow, d.date(), userid)
            elif kmlRow and overwrite:
                self.buildKML(kmlRow, d.date(), userid)
            elif len(kmlRow.f_kml) == 0 or len(kmlRow.f_kmz) == 0:
                self.buildKML(kmlRow, d.date(), userid)
            else:
                continue
        return None

    def daterange(self, start_date, end_date):
        for n in range(int((end_date - start_date).days+2)):
            yield start_date + timedelta(days=n)

    def takeColor(self):
        colors = {
            'aqua': 'ffffff00',
            'black': 'ff000000',
            'blue': 'ffff0000',
            'blueviolet': 'ffe22b8a',
            'brown': 'ff2a2aa5',
            'burlywood': 'ff87b8de',
            'cadetblue': 'ff726853',
            'chocolate': 'ff1e69d2',
            'coral': 'ff507fff',
            'crimson': 'ff3c14dc',
            'darkgreen': 'ff203201',
            'fuchsia': 'ffff00ff',
            'gold': 'ff0070ff',
            'gray': 'ff808080',
            'khaki': 'ff91b0c3',
            'lime': 'ff00ffbf',
            'pink': 'ffcbc0ff',
            'red': 'ff0000ff',
            'sea': 'ff946900',
            'yellowgreen': 'ff32cd9a'}
        return colors[self.color]

    def buildKML(self, row, date, userid):
        db = self.db
        from lxml import etree
        from pykml.factory import KML_ElementMaker as KML

        start = datetime.combine(date, time(
            second=0))
        stop = datetime.combine(date, time(
            hour=23, minute=59, second=59))

        samples = db((db.samples.modified_on > start) &
                     (db.samples.modified_on < stop) &
                     (db.samples.modified_by == userid)).select(
            orderby=db.samples.f_timestamp)

        if len(samples) < 5:
            return

        # **********************************************************
        # make map file
        # **********************************************************
        main = KML.Folder(KML.name(str(date)))
        main.append(KML.Style(
            KML.IconStyle(
                KML.color(self.takeColor()),
                KML.scale(0.5),
                KML.Icon(
                    KML.href('http://gdzieonikurdesa.pl/static/images/sample.png')),
                KML.hotSpot(x="0.5", y="0.5",
                            xunits="fraction",
                            yunits="fraction"),
            ),
            id="sample"))
        main.append(KML.Style(
            KML.LineStyle(
                KML.color(self.takeColor()),
                KML.width(4)
            ),
            id="line"))

        # for s in samples:
        #     desc = '%s: %s\n' % (current.T('Timestamp'), s.f_timestamp)
        #     desc += '%s: %s [km/h]\n' % (current.T('Speed'), s.f_speed)
        #     desc += '%s: %s [m]\n' % (current.T('Accuracy'), s.f_accuracy)
        #     desc += '%s: %s\n' % (current.T('Time signature'), s.modified_on)
        #     main.append(KML.Placemark(
        #         KML.name(''),
        #         KML.description(desc.decode('utf-8')),
        #         KML.styleUrl('#sample'),
        #         KML.Point(
        #             KML.coordinates("%s, %s" % (
        #                 str(s.f_longitude),
        #                 str(s.f_latitude))))
        #     ))

        desc = '%s: %s\n' % ('Data: ', samples[0].modified_on)
        points = []
        for s in samples:
            points.append([s.f_longitude, s.f_latitude, 0])
        main.append(KML.Placemark(
            KML.name(''),
            KML.description(desc.decode('utf-8')),
            KML.styleUrl('#line'),
            KML.LineString(
                KML.coordinates(' '.join(
                    [str(item).strip('[]').replace(' ', '')
                     for item in points])))))

        import uuid
        signature = uuid.uuid4()
        filename_kml = current.request.folder + \
            '\\uploads\\%s.kml' % signature
        filename_kmz = current.request.folder + \
            '\\uploads\\%s.kmz' % signature
        import zipfile
        myzip = zipfile.ZipFile(filename_kmz, 'w', zipfile.ZIP_DEFLATED)
        myzip.writestr('map.kml', etree.tostring(main))
#         myzip.write(
#             current.request.folder + '\\static\\images\\sample.png',
#             'sample.png')
        myzip.close()

        kml = open(filename_kml, 'w')
        kml.write(etree.tostring(main))
        kml.close()

        saved_kml = db.kml.f_kml.store(open(filename_kml, 'rb'), filename_kml)
        saved_kmz = db.kml.f_kmz.store(open(filename_kmz, 'rb'), filename_kmz)

        if row:
            db(db.kml._id == row.id).update(
                f_samples_nb=len(samples),
                f_kml=saved_kml,
                f_kmz=saved_kmz)
        else:
            db.kml.insert(
                f_date=date,
                f_user_id=userid,
                f_samples_nb=len(samples),
                f_kml=saved_kml,
                f_kmz=saved_kmz)
        db.commit()

        import os
        os.remove(filename_kml)
        os.remove(filename_kmz)
