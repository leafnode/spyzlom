# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
## This is a sample controller
## - index is the default action of any application
## - user is required for authentication and authorization
## - download is for downloading files uploaded in the db (does streaming)
## - call exposes all registered services (none by default)
#########################################################################
#response.menu.append(auth.navbar())

import datetime
newZlombol = datetime.date(2015, 7, 10)

@cache(request.env.path_info, time_expire=60, cache_model=cache.ram)
def index():
    teams = len(db(db.auth_user.id > 0)
                (db.samples.created_by == db.auth_user.id)
                (db.samples.created_on > newZlombol).select(
                    db.auth_user.id,
                    groupby=db.auth_user.id,
                    cache=(cache.ram, 1800)))
    menu1 = response.menu[0:3]
    menu2 = response.menu[3:]
    response.menu = menu1 + \
        [('teams', False, None, [(
            'extra',
            INPUT(_id='searchbar',
                  _placeholder='%s (%s)' % (T('Search…'), teams)))])] + \
        menu2
    timer_timeout = 15000
    if request.user_agent().is_mobile:
        timer_timeout = 30000
    return dict(timer_timeout=timer_timeout)


def androidapp():
    return dict(qrcode=A(IMG(_src="http://qrfree.kaywa.com/?l=1&s=8&d=market%3A%2F%2Fdetails%3Fid%3Dpl.waw.gdzieonikurdesa.donosiciel",
                         _alt="QR Code", _style="width: 100%;"),
                         _href="https://play.google.com/store/apps/details?id=pl.waw.gdzieonikurdesa.donosiciel"),
                a=A(IMG(_src=URL('static', 'images/play.png')),
                    _class='btn contact first',
                    _href="https://play.google.com/store/apps/details?id=pl.waw.gdzieonikurdesa.donosiciel"))


@cache(request.env.path_info + str(auth.user_id) or "",
       time_expire=1200, cache_model=cache.ram)
def help():
    data = {}
    if auth.user:
        user_id = auth.user.id
        team = db(db.auth_user.id == auth.user).select(
            db.auth_user.f_team).first().f_team
    else:
        user_id = T('TEAM_ID')
        team = ''
    data['iframe'] = '<iframe style=\"width: 100%%; height:640px;\" src=\"%s\"></iframe>' % URL('default', '/#%s' % user_id, host=True)
    data['direct_link'] = URL('default', '/#%s' % user_id, host=True)
    data['team'] = team
    return dict(content=data)


def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/manage_users (requires membership in
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    """
    return dict(form=auth())


@cache.action()
def download():
    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()


@auth.requires_login()
def data():
    """
    http://..../[app]/default/data/tables
    http://..../[app]/default/data/create/[table]
    http://..../[app]/default/data/read/[table]/[id]
    http://..../[app]/default/data/update/[table]/[id]
    http://..../[app]/default/data/delete/[table]/[id]
    http://..../[app]/default/data/select/[table]
    http://..../[app]/default/data/search/[table]
    but URLs must be signed, i.e. linked with
      A('table',_href=URL('data/tables',user_signature=True))
    or with the signed load operator
      LOAD('default','data.load',args='tables',ajax=True,user_signature=True)
    """
    return dict(form=crud())


@auth.requires_login()
@request.restful()
def api():
    response.view = 'generic.'+request.extension

    def GET(*args, **vars):
        patterns = 'auto'
        parser = db.parse_as_rest(patterns, args, vars)
        if parser.status == 200:
            return dict(content=parser.response)
        else:
            raise HTTP(parser.status, parser.error)

    def POST(table_name, **vars):
        return db[table_name].validate_and_insert(**vars)

    def PUT(table_name, **vars):
        return db[table_name].validate_and_update(**vars)

    # def DELETE(*args, **vars):
    #     return db(db[table_name]._id == vars[id]).delete()
    return locals()


@auth.requires_login()
@request.restful()
def uapi():
    response.view = 'generic.'+request.extension

    def GET(*args, **vars):
        if args[0] == 'me':
            if args[1] == 'failure':
                result = db(db.auth_user.id == auth.user).select(
                    db.auth_user.id,
                    db.auth_user.f_failure,
                    db.auth_user.f_failure_desc)
                return dict(content=result)

    def PUT(**vars):
        return db(db.auth_user._id == auth.user).validate_and_update(**vars)

    return locals()


@auth.requires_login()
def getLastValidSample():
    lastSample = (db((db.samples.id > 0) & (db.samples.created_by == auth.user)
                     ).select(orderby=~db.samples.f_timestamp, limitby=(0, 1)
                              ).last())
    if not lastSample:
        return 0
    else:
        return lastSample.f_timestamp


@auth.requires_login()
def pushSamples():
    import gluon.contrib.simplejson
    data = gluon.contrib.simplejson.loads(request.body.read())
    for d in data:
        db.samples.validate_and_insert(**d)


# @cache(request.env.path_info, time_expire=60, cache_model=cache.ram)
# @cache.action(time_expire=60, cache_model=cache.ram, session=False, vars=False, public=True)
def getTeamsLastPos():
    response.view = 'generic.' + request.extension
    last = db.samples.f_timestamp.max()
    q0 = (db.samples.modified_on > newZlombol)
    q1 = (db(db.samples.created_by == db.auth_user.id)
          (db.samples.id > 0)(q0)._select(last, groupby=db.auth_user.id))
    q2 = (db(db.samples.created_by == db.auth_user.id)
            (db.samples.f_timestamp.belongs(q1)).select(
                db.auth_user.id,
                db.auth_user.f_team,
                db.auth_user.f_color,
                db.auth_user.f_vehicle_type,
                db.auth_user.f_failure,
                db.samples.modified_on,
                db.samples.f_timestamp,
                db.samples.f_longitude,
                db.samples.f_latitude, cache=(cache.ram, 10)))
    data = dict()
    for r in q2:
        diff = datetime.datetime.now() - r.samples.modified_on
        state = 'active'
        if diff.total_seconds() > 900:
            state = 'idle'
        if r.auth_user.f_failure:
            state = 'failure'
        user_data = dict(
            team='[#%d] ' % r.auth_user.id + r.auth_user.f_team,
            color=r.auth_user.f_color,
            type=r.auth_user.f_vehicle_type,
            id=r.auth_user.id,
            state=state,
            lastPos=r.samples,
            modified_on=r.samples.modified_on)
        data[r.auth_user.id] = user_data

    return data


def getTeams():
    words = request.vars['words'].lower().split()

    response.view = 'generic.' + request.extension
    last = db.samples.f_timestamp.max()
    q0 = (db.samples.modified_on > newZlombol)
    if len(words) > 0:
        q00 = None
        for w in words:
            query = '%%%s%%' % w
            q001 = db.auth_user.f_team.lower().like(query)
            if q00:
                q00 |= q001
            else:
                q00 = q001
            q00 |= db.auth_user.f_vehicle.lower().like(query)
            q00 |= db.auth_user.f_city.lower().like(query)
        q0 &= q00

    q1 = (db(db.samples.created_by == db.auth_user.id)
          (db.samples.id > 0)(q0)._select(last, groupby=db.auth_user.id))
    q2 = (db(db.samples.created_by == db.auth_user.id)
            (db.samples.f_timestamp.belongs(q1)).select(
                db.auth_user.id,
                db.auth_user.f_team,
                db.auth_user.f_color,
                db.auth_user.f_vehicle_type,
                orderby=db.auth_user.f_team.lower(),
                cache=(cache.ram, 15)))
    data = TAG['']()
    for r in q2:
        img = IMG(_src=URL('static', 'icons/%s/%s.png' % (r.f_color,
                                                          r.f_vehicle_type)))
        data.append(LI(A(img, ' ', r.f_team, _id=r.id, _class="team",
                    _href='#')))

    return dict(content=data)



def getTeamInfo():
    response.view = 'generic.' + request.extension
    team = request.vars['id']
    last = db.samples.f_timestamp.max()
    q0 = db(db.samples.modified_by == team)._select(last)
    qLast = db(db.samples.f_timestamp.belongs(q0)).select(
        db.samples.modified_on,
        db.samples.f_speed).first()
    q1 = db(db.auth_user.id == team).select(
        db.auth_user.id,
        db.auth_user.f_team,
        db.auth_user.f_vehicle,
        db.auth_user.f_city,
        db.auth_user.f_www,
        db.auth_user.f_funpage,
        db.auth_user.f_crew_size,
        db.auth_user.f_contact,
        db.auth_user.f_contact_phone,
        db.auth_user.f_contact_email,
        db.auth_user.f_thumb_small,
        db.auth_user.f_thumb_big,
        db.auth_user.f_failure,
        db.auth_user.f_failure_desc,
        cache=(cache.disk, 60)).first()

    row = DIV(_class='span12')
    container = TAG['']()
    desc = TAG['dl'](_class='span8 dl-horizontal')

    delta = datetime.datetime.now() - qLast.modified_on
    last_desc = ''
    last_speed = '?'
    if delta.days:
        last_desc += T('%s days ago') % delta.days
    else:
        last_desc += T('%s hours %s minutes ago') % (delta.seconds//3600,
                                                     (delta.seconds//60) % 60)
        if qLast.f_speed > 0:
            last_speed = '%.1f [km/h]' % qLast.f_speed
    if q1.f_thumb_small:
        if request.user_agent().is_mobile:
            container.append(A(T(
                "Show team picture"),
                _href=URL('default', 'download/' + q1.f_thumb_big),
                _class='btn fancybox span4'))
        else:
            container.append(A(IMG(
                _src=URL('default', 'download/' + q1.f_thumb_small),
                _style="width: 100%;",
                _class="img-polaroid"),
                _href=URL('default', 'download/' + q1.f_thumb_big),
                _class='fancybox span4'))

    if q1.f_failure_desc:
        desc.append(TAG['blockquote'](
            P((T('Problem description'))),
            TAG['small'](q1.f_failure_desc),
            _class='failure'))
    if q1.f_contact:
            desc.append(TAG['blockquote'](
                P((T('From you to others'))),
                TAG['small'](q1.f_contact)))
    desc.append(TAG['dt'](T("Last update")))
    desc.append(TAG['dd'](last_desc))
    desc.append(TAG['dt'](T("Speed")))
    desc.append(TAG['dd'](last_speed))
    desc.append(TAG['dt'](T("Crew")))
    desc.append(TAG['dd'](q1.f_team))
    desc.append(TAG['dt'](T("Team ID")))
    desc.append(TAG['dd'](q1.id))
    if q1.f_city:
        desc.append(TAG['dt'](T("City of origin")))
        desc.append(TAG['dd'](q1.f_city))
    desc.append(TAG['dt'](T("Vehicle")))
    desc.append(TAG['dd'](q1.f_vehicle))
    if q1.f_crew_size:
        desc.append(TAG['dt'](T("Crew members")))
        desc.append(TAG['dd'](q1.f_crew_size))
    if q1.f_www:
        desc.append(TAG['dt'](T("Webpage")))
        desc.append(TAG['dd'](A(T('open in new tab'),
                    _href=q1.f_www, _target='blank')))
    if q1.f_funpage:
        desc.append(TAG['dt'](T("Fanpage")))
        desc.append(TAG['dd'](A(T('open in new tab'),
                    _href=q1.f_funpage, _target='blank')))
    if auth.is_logged_in():
        if q1.f_contact_email:
            desc.append(A(I(_class='fa fa-envelope'), '  ',
                          q1.f_contact_email,
                          _class='btn contact first',
                          _href="mailto:%s" % q1.f_contact_email))
        if q1.f_contact_phone:
            desc.append(A(I(
                _class='fa fa-phone'), '  ',
                q1.f_contact_phone,
                _class='btn contact',
                _href="tel:%s" % q1.f_contact_phone.replace(' ', '')))

    container.append(desc)
    row.append(container)
    return dict(content=row)


def getTeamMaps():
    response.view = 'generic.' + request.extension
    team = request.vars['id']
    rows = db(db.kml.f_user_id == team).select(
        orderby=~db.kml.f_date,
        cache=(cache.disk, 120))

    if request.extension == 'json':
        return dict(content=rows)
    tag = TABLE(_class='table table-condensed')
    tbody = TBODY()
    tag.append(tbody)
    for r in rows:
        tr = TR()
        tr.append(str(r.f_date))
        tr.append(T("Samples Nb: %s") % (r.f_samples_nb or '?'))
        tr.append(A(T('Show/Hide'), **{
            '_class': 'btn mapToggle',
            '_data-mapid': r.id,
            '_data-filename': r.f_kml
        }))
        tr.append(A(T('Download *.kmz'),
                    _class='btn',
                    _href=URL('default', 'download/%s' % r.f_kmz)
        ))
        tbody.append(tr)
    return tag


def getDownloader():
    #response.view = 'generic.' + request.extension
    team = request.vars['id']
    return dict(team=team)


def getSamples():
    import cStringIO
    from datetime import datetime
    from datetime import timedelta
    import time
    #response.view = 'generic.html'
    user = request.args[0]

    try:
        f_from = datetime.strptime(request.vars['f_from'], "%Y-%m-%d")
        f_to = datetime.strptime(request.vars['f_to'], "%Y-%m-%d")
    except:
        f_from = datetime.now() - timedelta(days=30)
        f_to = datetime.now()

    f_from_t = int(time.mktime(f_from.timetuple()) * 1000)
    f_to_t = int(time.mktime(f_to.timetuple()) * 1000)

    samples = db((db.samples.f_timestamp > f_from_t) &
                 (db.samples.f_timestamp < f_to_t) &
                 (db.samples.modified_by == user)).select(
                    db.samples.id,
                    db.samples.f_timestamp,
                    db.samples.f_latitude,
                    db.samples.f_longitude,
                    db.samples.f_altitude,
                    db.samples.f_speed,
                    db.samples.f_accuracy,
                    db.samples.f_bearing,
                    db.samples.f_provider,
                    db.samples.created_on)

    return dict(user=user, samples=samples)
